package com.example.demo.user.exceptions;
/*
User Exception( means exception created in our own)
 */
public class UserServiceException extends RuntimeException{
    public UserServiceException(String message) {
        super(message);
    }
}
