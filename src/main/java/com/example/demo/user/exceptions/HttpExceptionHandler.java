package com.example.demo.user.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class HttpExceptionHandler {
    @ExceptionHandler(value = {UserServiceException.class}) //here we can use more than one exception class
    //automatically call this class from exception class(UserServiceException.class) using this annotation
    /*
    Sending only errorMessage in oneLine
     */
//    public ResponseEntity<Object> HandleUserServiceException(UserServiceException ex, WebRequest webRequest){
//        return new ResponseEntity<>(ex.getMessage(),new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
//    }

    /*
    Sending response JSON type but it created by us
     */
    public ResponseEntity<Object> HandleUserServiceException(UserServiceException ex, WebRequest webRequest){
        ErrorMessageJSONType errorMessageJSONType=new ErrorMessageJSONType(new Date(),ex.getMessage());
        return new ResponseEntity<>(errorMessageJSONType,new HttpHeaders(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
    /*
    handle other exceptions
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> HandleOtherExceptionException(Exception ex, WebRequest webRequest){
        ErrorMessageJSONType errorMessageJSONType=new ErrorMessageJSONType(new Date(),ex.getMessage());
        return new ResponseEntity<>(errorMessageJSONType,new HttpHeaders(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
