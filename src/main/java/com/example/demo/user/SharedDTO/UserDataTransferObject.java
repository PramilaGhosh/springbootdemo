package com.example.demo.user.SharedDTO;

import java.io.Serializable;
import java.util.List;

//this class directly communicate with request model class as well as response model class
// also entity class can communicate to this class
public class UserDataTransferObject implements Serializable {
    private static final long serialVersionUID= 5403146047016371212L;

    private long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String encryptedPassword;
    private String mailVerificationToken;
    private String mailVerificationStatus;
    private List<AddressDataTransferObject> address;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public String getMailVerificationToken() {
        return mailVerificationToken;
    }

    public void setMailVerificationToken(String mailVerificationToken) {
        this.mailVerificationToken = mailVerificationToken;
    }

    public String getMailVerificationStatus() {
        return mailVerificationStatus;
    }

    public void setMailVerificationStatus(String mailVerificationStatus) {
        this.mailVerificationStatus = mailVerificationStatus;
    }
    public List<AddressDataTransferObject> getAddress() {
        return address;
    }

    public void setAddress(List<AddressDataTransferObject> address) {
        this.address = address;
    }
}
