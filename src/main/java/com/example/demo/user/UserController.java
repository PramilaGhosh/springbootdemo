package com.example.demo.user;

import com.example.demo.user.SharedDTO.UserDataTransferObject;
import com.example.demo.user.requestModel.UserDetailsRequestModel;
import com.example.demo.user.response.OperationStatusModel;
import com.example.demo.user.response.RequestOperationStatus;
import com.example.demo.user.response.UserResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping(path="/{id}",
    produces={MediaType.APPLICATION_XML_VALUE})
    public UserResponse getMapping(@PathVariable String id) throws Exception {

        UserResponse returnValue=new UserResponse();
        UserDataTransferObject userDataTransferObject= userService.getUserByUserId(id);
        BeanUtils.copyProperties(userDataTransferObject,returnValue);

        return returnValue;
    }
    @PostMapping
    public UserResponse postMapping(@RequestBody UserDetailsRequestModel userDetailsRequestModel) {
//        UserResponse returnValue=new UserResponse();
//        UserDataTransferObject userDataTransferObject=new UserDataTransferObject();
//        BeanUtils.copyProperties(userDetailsRequestModel,userDataTransferObject);
        ModelMapper modelMapper=new ModelMapper();

        UserDataTransferObject userDataTransferObject=modelMapper.map(userDetailsRequestModel, UserDataTransferObject.class);
        System.out.println("===========Post==============");
        UserDataTransferObject createdUser= userService.createUser(userDataTransferObject);
//        BeanUtils.copyProperties(createdUser,returnValue);
        UserResponse returnValue= modelMapper.map(createdUser,UserResponse.class);

        return returnValue;
    }
    @PutMapping(path="/{id}")
    public UserResponse putMapping(@PathVariable String id, @RequestBody UserDetailsRequestModel userDetailsRequestModel) {
        UserResponse returnValue=new UserResponse();
        UserDataTransferObject userDataTransferObject=new UserDataTransferObject();
        BeanUtils.copyProperties(userDetailsRequestModel,userDataTransferObject);
        UserDataTransferObject updateUser=userService.updateUser(id,userDataTransferObject);
        BeanUtils.copyProperties(updateUser,returnValue);
        return returnValue;
    }
    @DeleteMapping(path = "/{id}")
    public OperationStatusModel deleteMapping(@PathVariable String id) {
        OperationStatusModel returnValue=new OperationStatusModel();
        System.out.println("Delete operation");
        userService.deleteUser(id);
        returnValue.setOperationName(RequestOperationNameStatus.DELETE.name()); // name() convert enum to string
        returnValue.setOperationStatus(RequestOperationStatus.SUCCESS.name());
        System.out.println(returnValue.getOperationName());
        System.out.println(returnValue.getOperationStatus());
        return returnValue;
    }
    @GetMapping  //API Call (localhost:8080/?page=0&limit=12)
    public List<UserResponse> getUsers(@RequestParam (value="page", defaultValue="0")int page,
                                       @RequestParam(value = "limit",defaultValue = "5")int limit){
        List<UserResponse> returnValue=new ArrayList<>();
        List<UserDataTransferObject> getUsers=userService.getUsers(page,limit);
       for(UserDataTransferObject user:getUsers){
           UserResponse userResponse=new UserResponse();
           BeanUtils.copyProperties(user,userResponse);
           returnValue.add(userResponse);
       }
        return returnValue;
    }

}