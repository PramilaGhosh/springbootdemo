package com.example.demo.user;

import com.example.demo.user.SharedDTO.UserDataTransferObject;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    UserDataTransferObject createUser(UserDataTransferObject userDataTransferObject);
    UserDataTransferObject getUserByUserId(String id) throws Exception;

    UserDataTransferObject updateUser(String id, UserDataTransferObject userDataTransferObject);

    void deleteUser(String id);

    List<UserDataTransferObject> getUsers(int page, int limit);
}
