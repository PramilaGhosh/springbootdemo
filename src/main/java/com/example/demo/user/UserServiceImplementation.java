package com.example.demo.user;

import com.example.demo.user.SharedDTO.AddressDataTransferObject;
import com.example.demo.user.SharedDTO.UserDataTransferObject;
import com.example.demo.user.Utils.UtilsUserId;
import com.example.demo.user.exceptions.UserServiceException;
import org.apache.tomcat.jni.Address;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import java.util.ArrayList;
import java.util.List;

import static com.example.demo.user.response.ErrorMessages.*;

@Component
public class UserServiceImplementation implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UtilsUserId utilsUserId;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public UserDataTransferObject createUser(UserDataTransferObject userDataTransferObject) throws RuntimeException{
        if(userDataTransferObject.getEmail()=="") throw new NullPointerException("Please put mail id");
        if(userRepository.findByEmail(userDataTransferObject.getEmail())!=null)
            throw new UserServiceException(MAIL_ALREADY_EXIST.getErrorMessage());
//            throw new NullPointerException("Null");}
//        UserEntity userEntity=new UserEntity();
//        BeanUtils.copyProperties(userDataTransferObject,userEntity);
        for(int i=0;i<userDataTransferObject.getAddress().size();i++){

            AddressDataTransferObject addressDataTransferObject=userDataTransferObject.getAddress().get(i);

            addressDataTransferObject.setAddressId(utilsUserId.generateAddressId(16));
            addressDataTransferObject.setUserDetails(userDataTransferObject);
            userDataTransferObject.getAddress().set(i,addressDataTransferObject); //replace userDataTransferObject.getAddress()[i] with  addressDataTransferObject.
              }
        ModelMapper modelMapper=new ModelMapper();
        UserEntity userEntity =modelMapper.map(userDataTransferObject,UserEntity.class);

        userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(userDataTransferObject.getPassword()));
        userEntity.setUserId(utilsUserId.generateUserId(16));

        userEntity.setMailVerificationStatus("true");
        UserEntity storedUserDetails= userRepository.save(userEntity);
//        AddressEntity addressEntity=modelMapper.map(userDataTransferObject.getAddress().get(i),AddressEntity.class);

//        BeanUtils.copyProperties(storedUserDetails,returnValue);
        UserDataTransferObject returnValue=modelMapper.map(storedUserDetails,UserDataTransferObject.class);
        return returnValue;
    }

    @Override
    public UserDataTransferObject getUserByUserId(String id) {
        UserDataTransferObject returnValue=new UserDataTransferObject();
        UserEntity userDetails= userRepository.findByUserId(id);
        if(userDetails==null) {
            throw new UserServiceException(USER_ID_NOT_MATCHED.getErrorMessage());
//            throw new userNameNotFoundException();
        }
        BeanUtils.copyProperties(userDetails,returnValue);
        return returnValue;
    }

    @Override
    public UserDataTransferObject updateUser(String id, UserDataTransferObject userDataTransferObject) {
        if(userRepository.findByUserId(id)==null) throw new UserServiceException(USER_ID_NOT_MATCHED.getErrorMessage());
        UserDataTransferObject returnValue=new UserDataTransferObject();
        UserEntity userDetails=userRepository.findByUserId(id);
        userDetails.setFirstName(userDataTransferObject.getFirstName());
        userDetails.setLastName(userDataTransferObject.getLastName());
        UserEntity updateUser=userRepository.save(userDetails);
        BeanUtils.copyProperties(updateUser,returnValue);
        return returnValue;
    }

    @Override
    public void deleteUser(String id) {
        UserEntity userDetails=userRepository.findByUserId(id);
        if(userDetails==null) throw new UserServiceException(USER_ID_NOT_FOUND.getErrorMessage());
        userRepository.delete(userDetails);


    }

    @Override
    public List<UserDataTransferObject> getUsers(int page, int limit) {
        if(page>0) page-=1;
        List<UserDataTransferObject> returnValue=new ArrayList<>();
        Pageable pageableRequest= PageRequest.of(page,limit);
        Page<UserEntity> userPage=userRepository.findAll(pageableRequest);
        List<UserEntity> users=userPage.getContent();
        for(UserEntity user: users){
            UserDataTransferObject userDto= new UserDataTransferObject();
            BeanUtils.copyProperties(user,userDto);
            returnValue.add(userDto);
        }
        return returnValue;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return null;
    }
}
