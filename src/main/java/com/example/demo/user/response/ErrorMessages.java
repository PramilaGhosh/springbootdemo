package com.example.demo.user.response;

public enum ErrorMessages {
    MISSING_REQUIRED_FIELD("Missing required field,Please check documentation for required field"),
    RECORD_ALREADY_EXIT("Record already exit"),
    INTERNAL_SERVER_ERROR("Internal server error"),
    MAIL_ALREADY_EXIST("Mail already exist"),
    USER_ID_NOT_MATCHED("User id is not matched, Please check once"),

    USER_ID_NOT_FOUND("User id is not found");

    private final String errorMessage;

    ErrorMessages(String errorMessage) {

        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
