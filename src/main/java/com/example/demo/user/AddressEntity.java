package com.example.demo.user;

import com.ctc.wstx.shaded.msv.org_isorelax.dispatcher.impl.IgnoreVerifier;
import com.example.demo.user.SharedDTO.UserDataTransferObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import net.bytebuddy.agent.builder.AgentBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "address")
public class AddressEntity implements Serializable {

    private static final long serialVersionUID = -3373945673127742944L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false,length = 50)
    private String addressId;
    @Column(nullable = false,length = 50)
    private String city;
    @Column(nullable = false,length = 50)
    private String country;
    @Column(nullable = false,length = 50)
    private String streetName;
    @Column(nullable = false,length = 50)
    private String postalCode;
    @ManyToOne(targetEntity = UserEntity.class)// if we not put targetEntity then it gives error like" org.hibernate.AnnotationException: @OneToOne or @ManyToOne on com.example.demo.user.AddressEntity.userDetails
                  // if we not put targetEntity then it gives error like" org.hibernate.AnnotationException: @OneToOne or @ManyToOne on com.example.demo.user.AddressEntity.userDetails
                                                // references an unknown entity: com.example.demo.user.SharedDTO.UserDataTransferObject'
//    @ElementCollection(fetch = FetchType.LAZY)
    @JoinColumn(name = "id",insertable = false,updatable = false,referencedColumnName = "id")
    private UserDataTransferObject userDetails;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public UserDataTransferObject getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDataTransferObject userDetails) {
        this.userDetails = userDetails;
    }
}
