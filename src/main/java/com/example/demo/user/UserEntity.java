package com.example.demo.user;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name="users")
public class UserEntity implements Serializable {

    private static final long serialVersionUID= 5403146047016371212L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  long id;
    @Column(nullable=false,length=50)
    private String userId;
    @Column(nullable=false,length=50)
    private String firstName;
    @Column(nullable=false,length=50)
    private String lastName;
    @Column(nullable=false,length=100,unique = true)
    private String email;
    @Column(nullable=false)
    private String encryptedPassword;
    private String mailVerificationToken;
    @Column
    private String mailVerificationStatus;

//    @OneToMany(targetEntity = AddressEntity.class, mappedBy = "userDetails",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
//    private List<AddressEntity> address;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public String getMailVerificationToken() {
        return mailVerificationToken;
    }

    public void setMailVerificationToken(String mailVerificationToken) {
        this.mailVerificationToken = mailVerificationToken;
    }

    public String getMailVerificationStatus() {
        return mailVerificationStatus;
    }

    public void setMailVerificationStatus(String mailVerificationStatus) {
        this.mailVerificationStatus = mailVerificationStatus;
    }
//    public List<AddressEntity> getAddress() {
//        return address;
//    }
//
//    public void setAddress(List<AddressEntity> address) {
//        this.address = address;
//    }

}
