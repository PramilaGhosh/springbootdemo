package com.example.demo.security;

import com.example.demo.user.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
@EnableWebSecurity
public class WebSecurity {
    private final UserService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurity(UserService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    @Bean
    protected SecurityFilterChain configure(HttpSecurity http) throws Exception{
        AuthenticationManagerBuilder authenticationManagerBuilder=http.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);
        http.csrf().disable()
                .authorizeRequests().antMatchers(HttpMethod.POST,"/").permitAll()
                .antMatchers(HttpMethod.GET,"/{id}").permitAll()
                .antMatchers(HttpMethod.PUT,"/{id}").permitAll()
                .antMatchers(HttpMethod.DELETE,"/{id}").permitAll()
                .antMatchers(HttpMethod.GET,"/").permitAll()   //for Paging get request
                .anyRequest().authenticated();
        return http.build();
    }
}
