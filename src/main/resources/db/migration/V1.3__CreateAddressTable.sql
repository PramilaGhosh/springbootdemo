CREATE TABLE ADDRESS
(
ID      BIGINT GENERATED BY DEFAULT AS IDENTITY,
address_id VARCHAR(50) NOT NULL,
CITY VARCHAR(20) NOT NULL,
country VARCHAR(50) NOT NULL,
street_name VARCHAR(50) NOT NULL,
postal_code VARCHAR(7) NOT  NULL,
user_id INT REFERENCES users (id),
PRIMARY KEY(ID)
)