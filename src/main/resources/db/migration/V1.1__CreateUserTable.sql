CREATE TABLE users
(
    id       BIGINT GENERATED BY DEFAULT AS IDENTITY,
    user_id     VARCHAR(50) NOT NULL,
    first_name     VARCHAR(50) NOT NULL,
    last_name     VARCHAR(50) NOT NULL,
    email     VARCHAR(50) NOT NULL UNIQUE,
    encrypted_password  VARCHAR(100) NOT NULL,
    mail_verification_token  VARCHAR(100),
    mail_verification_status VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);
